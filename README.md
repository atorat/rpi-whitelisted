
This is for setting up a Raspberry Pi to be suitable for very curious kids.  It
is based on Raspberry Pi OS 64-bit Lite, to provide the base layer with all the
right drivers and graphics support.  It installs GNOME from Debian rather than
the Raspberry Pi environment.  The general idea is to make it as Debian-ish as
possible, but that provides a full, uncrippled work environment.

In order to run this remotely, start SSH on the Raspberry Pi first:

    sudo service ssh start

Fetch ansible dependencies:

    ansible-galaxy install -f -r requirements.yml -p .galaxy

Then you can immediately start the deployment:

    ansible-playbook -i remoteHost, provision.yml


# Troubleshooting

## If audacity can't find the audio:

https://www.raspberrypi.org/forums/viewtopic.php?p=1673934#p1673934

```console
# printf ' snd_bcm2835.enable_headphones=0 snd_bcm2835.enable_hdmi=0 snd_bcm2835.enable_compat_alsa=1' >> /boot/cmdline.txt
# reboot
$ rm -f ~/.asoundrc
$ amixer cset numid=3 1
```


## To force the screen resolution

In _/boot/config.txt_:
```conf
hdmi_group=2
hdmi_mode=58
```

Some example modes are here https://github.com/raspberrypi/firmware/issues/4
