---
- hosts: all
  become_user: pi
  remote_user: root

  vars:
    user: "pi"
    userhome: "/home/{{ user }}"
    hostname: "{{ inventory_hostname.split('.')[0] }}"
    keyrings: "/usr/local/share/keyrings"
    ansible_python_interpreter: /usr/bin/python3

  tasks:

    - name: "enable SSH daemon start at boot"
      systemd:
        name: ssh.service
        enabled: yes
      when: CI is undefined or not CI

    - name: "set /etc/hostname"
      copy:
        content: |
          {{ hostname }}
        dest: /etc/hostname
        mode: 0644
        owner: root
        group: root
      when: CI is undefined or not CI

    - name: "set full hostname with domain name"
      lineinfile:
        line: "127.0.1.1	{{ inventory_hostname }}.at.or.at	{{ inventory_hostname }}"
        dest: /etc/hosts
        state: present
      when: CI is undefined or not CI

    - name: "file: {{ userhome }}/.ssh directory"
      file:
        dest: "{{ userhome }}/.ssh"
        state: directory
        mode: 0700
        owner: "{{ user }}"
        group: "{{ user }}"
    - name: "copy: ssh access to {{ user }} account"
      copy:
        remote_src: true
        src: /root/.ssh/authorized_keys
        dest: "{{ userhome }}/.ssh/authorized_keys"
        mode: 0700
        owner: "{{ user }}"
        group: "{{ user }}"
      tags: policies

    - name: "apt_repository: bullseye"
      apt_repository:
        repo: 'deb https://debian.anexia.at/debian {{ ansible_distribution_release }} main'
        state: present
        update_cache: no

    - name: "apt_repository: bullseye-updates"
      apt_repository:
        repo: 'deb https://debian.anexia.at/debian {{ ansible_distribution_release }}-updates main'
        state: present
        update_cache: no

    - name: "apt_repository: bullseye-backports"
      apt_repository:
        repo: 'deb https://debian.anexia.at/debian {{ ansible_distribution_release }}-backports main'
        state: present
        update_cache: no

    - name: "apt_repository: bullseye-security"
      apt_repository:
        repo: "deb https://debian.anexia.at/debian-security {{ ansible_distribution_release }}-security main contrib non-free"
        state: present
        update_cache: no

    - name: "apt_repository: security.debian.org"
      apt_repository:
        repo: "deb https://security.debian.org/debian-security {{ ansible_distribution_release }}-security main contrib non-free"
        state: present
        update_cache: no

    - name: "file: creating keyrings directory"
      file:
        path: "{{ keyrings }}"
        state: directory
        mode: 0755
        owner: root
        group: root
    - name: "get_url: get archive.raspberrypi.org repo keys"
      get_url:
        url: https://archive.raspberrypi.org/debian/raspberrypi.gpg.key
        dest: "{{ keyrings }}"
        mode: 0755
        owner: root
        group: root
        checksum: sha256:76603890d82a492175caf17aba68dc73acb1189c9fd58ec0c19145dfa3866d56
    - name: "shell: convert keyring to .gpg"
      shell: |
        gpg --list-key
        gpg --homedir /tmp --import raspberrypi.gpg.key
        gpg --homedir /tmp/ --export > archive.raspberrypi.org-keyring.gpg
        gpg --keyring ./archive.raspberrypi.org-keyring.gpg --fingerprint CF8A1AF502A2AA2D763BAE7E82B129927FA3303E
      args:
        chdir: "{{ keyrings }}"
        creates: "{{ keyrings }}/archive.raspberrypi.org-keyring.gpg"

    - name: "apt_repository: archive.raspberrypi.org"
      apt_repository:
        repo: 'deb [signed-by={{ keyrings }}/archive.raspberrypi.org-keyring.gpg] https://archive.raspberrypi.org/debian/ {{ ansible_distribution_release }} main'
        state: present
        update_cache: no

    - name: "file: delete /etc/apt/sources.list.d/raspi.list"
      file:
        path: /etc/apt/sources.list.d/raspi.list
        state: absent

    - name: "file: delete /etc/apt/sources.list"
      file:
        path: /etc/apt/sources.list
        state: absent

    - name: "apt: dist-upgrade"
      apt:
        update_cache: yes
        upgrade: dist

    # this assumes using Raspberry Pi OS Lite
    - name: "apt: install GNOME packages"
      apt:
        install_recommends: yes
        update_cache: no
        name:
          - gnome
          - gnome-core
          - gnome-shell
          - gnome-shell-extension-weather
          - gnome-software
          - gnome-tweak-tool
          - network-manager
          - network-manager-gnome
          - piwiz
          - rpi-chromium-mods
          - xorg

    # https://github.com/TheMaroonHatHacker/gnomeforpi/blob/stable/gnomeforpi-install
    - name: "tweak startup settings"
      block:
        - name: "Get current systemd default"
          command: "systemctl get-default"
          changed_when: false
          register: systemdefault

        - name: "Set default to graphical target"
          command: "systemctl set-default graphical.target"
          when: "'graphical' not in systemdefault.stdout"

        - name: "enable NetworkManager"
          systemd:
            name: NetworkManager.service
            enabled: yes
      when: CI is undefined or not CI

    - name: "apt: install debian packages"
      apt:
        install_recommends: yes
        update_cache: no
        name:
          - apt-file
          - at
          - audacity
          - bash-completion
          - curl
          - dphys-swapfile
          - emacs-nox
          - etckeeper
          - figlet
          - gimp
          - git
          - git-gui
          - gitg
          - gitk
          - gmpc
          - htop
          - iotop
          - less
          - libreoffice
          - lincity
          - locales
          - micropolis
          - minetest
          - mousepad
          - mu-editor
          - ncdu
          - nextcloud-desktop
          - openssh-client
          - openssh-server
          - pd-chaos
          - pd-cyclone
          - pd-ekext
          - pd-fftease
          - pd-hcs
          - pd-iemutils
          - pd-jmmmp
          - pd-list-abs
          - pd-lyonpotpourri
          - pd-mapping
          - pd-markex
          - pd-maxlib
          - pd-pan
          - pd-pdstring
          - pd-readanysf
          - pd-zexy
          - p7zip-full
          - procps
          - psmisc
          - rsync
          - scratch
          - scratch3
          - screen
          - thonny
          - unattended-upgrades
          - unar
          - unrar-free
          - vim
          - vlc
          - wget

    - name: "copy: enable unattended-upgrades"
      copy:
        content: |
          APT::Periodic::Update-Package-Lists "1";
          APT::Periodic::Unattended-Upgrade "1";
        dest: /etc/apt/apt.conf.d/20auto-upgrades
        mode: 0644
        owner: root
        group: root

    - name: "apt: install flatpak packages"
      apt:
        install_recommends: yes
        update_cache: no
        default_release: bullseye-backports
        name:
          - flatpak
          - flatpak-builder

    - name: "apt: purge debian packages"
      apt:
        state: absent
        purge: yes
        update_cache: no
        name:
          - avrdude
          - avrdude:armhf
          - claws-mail
          - dhcpcd5
          - dillo
          - firefox
          - firefox-esr
          - gnome-orca
          - nano
          - openresolv
          - orca
          - termit

    - name: "timezone: set system"
      timezone:
        name: Europe/Vienna

    - name: 'locale_gen: generate needed locales'
      locale_gen:
        name: "{{ item }}"
        state: present
      with_items:
        - de_AT.UTF-8
        - de_DE.UTF-8
        - en_US.UTF-8
        - en_GB.UTF-8
    - name: 'lineinfile: set default system locale to en_US.UTF-8'
      lineinfile:
        dest: "/etc/default/locale"
        line: "LANG=en_US.UTF-8"

    - name: "copy: script for forcing screen resolution"
      copy:
        mode: 0700
        owner: "{{ user }}"
        group: "{{ user }}"
        content: |
          #!/bin/sh
          xrandr --newmode "1680x1050_60.00"  146.25  1680 1784 1960 2240  1050 1053 1059 1089 -hsync +vsync
          xrandr --addmode XWAYLAND-0 1680x1050_60.00
        dest: "{{ userhome }}/force-screen-resolution.sh"
    - name: "dconf: personalize settings"
      block:
        - dconf:
            key: /org/gnome/desktop/input-sources/xkb-options
            value:
              - 'altwin:alt_win'
              - 'caps:super'
              - 'compose:ralt'

        - dconf: key=/org/gnome/desktop/interface/clock-show-weekday value=true
        - dconf: key=/org/gnome/desktop/sound/allow-volume-above-100-percent value=true
        - dconf: key=/org/gnome/settings-daemon/plugins/color/night-light-enabled value=true
        - dconf: key=/org/gnome/settings-daemon/plugins/color/night-light-schedule-automatic value=false
        - dconf: key=/org/gnome/settings-daemon/plugins/color/night-light-temperature value=3450

        - dconf: key=/org/gnome/shell/keybindings/switch-to-application-1 value="['']"
        - dconf: key=/org/gnome/shell/keybindings/switch-to-application-2 value="['']"
        - dconf: key=/org/gnome/shell/keybindings/switch-to-application-3 value="['']"
        - dconf: key=/org/gnome/shell/keybindings/switch-to-application-4 value="['']"
        - dconf: key=/org/gnome/shell/keybindings/switch-to-application-5 value="['']"
        - dconf: key=/org/gnome/shell/keybindings/switch-to-application-6 value="['']"
        - dconf: key=/org/gnome/shell/keybindings/switch-to-application-7 value="['']"
        - dconf: key=/org/gnome/shell/keybindings/switch-to-application-8 value="['']"
        - dconf: key=/org/gnome/shell/keybindings/switch-to-application-9 value="['']"
        - dconf: key=/org/gnome/desktop/wm/keybindings/switch-to-workspace-1 value="['<Super>1']"
        - dconf: key=/org/gnome/desktop/wm/keybindings/switch-to-workspace-2 value="['<Super>2']"
        - dconf: key=/org/gnome/desktop/wm/keybindings/switch-to-workspace-3 value="['<Super>3']"
        - dconf: key=/org/gnome/desktop/wm/keybindings/switch-to-workspace-4 value="['<Super>4']"
        - dconf: key=/org/gnome/desktop/wm/keybindings/switch-to-workspace-5 value="['<Super>5']"
        - dconf: key=/org/gnome/desktop/wm/keybindings/switch-to-workspace-6 value="['<Super>6']"
        - dconf: key=/org/gnome/desktop/wm/keybindings/switch-to-workspace-7 value="['<Super>7']"
        - dconf: key=/org/gnome/desktop/wm/keybindings/switch-to-workspace-8 value="['<Super>8']"
        - dconf: key=/org/gnome/desktop/wm/keybindings/switch-to-workspace-9 value="['<Super>9']"
        - dconf: key=/org/gnome/desktop/wm/keybindings/switch-to-workspace-10 value="['<Super>0']"
        - dconf: key=/org/gnome/desktop/wm/keybindings/move-to-workspace-1 value="['<Super><Shift>1']"
        - dconf: key=/org/gnome/desktop/wm/keybindings/move-to-workspace-2 value="['<Super><Shift>2']"
        - dconf: key=/org/gnome/desktop/wm/keybindings/move-to-workspace-3 value="['<Super><Shift>3']"
        - dconf: key=/org/gnome/desktop/wm/keybindings/move-to-workspace-4 value="['<Super><Shift>4']"
        - dconf: key=/org/gnome/desktop/wm/keybindings/move-to-workspace-5 value="['<Super><Shift>5']"
        - dconf: key=/org/gnome/desktop/wm/keybindings/move-to-workspace-6 value="['<Super><Shift>6']"
        - dconf: key=/org/gnome/desktop/wm/keybindings/move-to-workspace-7 value="['<Super><Shift>7']"
        - dconf: key=/org/gnome/desktop/wm/keybindings/move-to-workspace-8 value="['<Super><Shift>8']"
        - dconf: key=/org/gnome/desktop/wm/keybindings/move-to-workspace-9 value="['<Super><Shift>9']"
        - dconf: key=/org/gnome/desktop/wm/keybindings/move-to-workspace-10 value="['<Super><Shift>0']"

        - dconf: key=/org/gnome/terminal/legacy/keybindings/prev-tab value="'<Alt>Left'"
        - dconf: key=/org/gnome/terminal/legacy/keybindings/next-tab value="'<Alt>Right'"
        - dconf: key=/org/gnome/terminal/legacy/keybindings/paste value="'<Primary>v'"
        - dconf: key=/org/gnome/terminal/legacy/keybindings/new-window value="'<Primary>n'"
        - dconf: key=/org/gnome/terminal/legacy/keybindings/new-tab value="'<Primary>t'"
        - dconf: key=/org/gnome/terminal/legacy/keybindings/move-tab-left value="'<Shift><Alt>Left'"
        - dconf: key=/org/gnome/terminal/legacy/keybindings/move-tab-right value="'<Shift><Alt>Right'"
      become_user: "{{ user }}"

    - name: "copy: script for updating with apt"
      copy:
        mode: 0700
        content: |
          #!/bin/sh

          set -x
          apt-get update
          apt-get -y dist-upgrade --download-only

          set -e
          apt-get -y upgrade
          apt-get dist-upgrade
          apt-get autoremove --purge
          apt-get clean
        dest: /root/update-all

    - name: "lineinfile: set swap to be bigger"
      lineinfile:
        path: /etc/dphys-swapfile
        regex: ^\s*.*CONF_SWAPSIZE=[0-9]+\s*$
        state: absent

    - name: "shell: set motd"
      shell: |
        echo > /etc/motd
        figlet {{ hostname }} >> /etc/motd
        printf '\ncreated with https://gitlab.com/atorat/rpi-whitelisted\n\n' >> /etc/motd

    - name: "known_hosts: trust github.com"
      known_hosts:
        path: /etc/ssh/ssh_known_hosts
        name: github.com
        key: "{{ lookup('file', 'github.com.known_hosts') }}"

    - name: "known_hosts: trust gitlab.com"
      known_hosts:
        path: /etc/ssh/ssh_known_hosts
        name: gitlab.com
        key: "{{ lookup('file', 'gitlab.com.known_hosts') }}"

    - name: "known_hosts: trust blinky.at.or.at"
      known_hosts:
        path: /etc/ssh/ssh_known_hosts
        name: blinky.at.or.at
        key: "{{ lookup('file', 'blinky.at.or.at.known_hosts') }}"

    - name: "lineinfile: delete all comments in /etc/ssh/sshd_config"
      lineinfile:
        path: /etc/ssh/sshd_config
        regex: ^ *#.*
        state: absent

    - name: "lineinfile: disable SSH password auth"
      lineinfile:
        path: /etc/ssh/sshd_config
        line: PasswordAuthentication no
        state: present

    - name: "lineinfile: allow root SSH without password"
      lineinfile:
        path: /etc/ssh/sshd_config
        line: PermitRootLogin prohibit-password
        state: present

    # https://www.chromium.org/administrators/linux-quick-start
    # https://www.chromium.org/administrators/policy-list-3
    - name: "file: chromium policies dir"
      file:
        dest: "/etc/chromium/policies/managed"
        state: directory
        mode: 0755
      tags: policies

    - name: "copy: set chromium policies"
      copy:
        mode: 0755
        src: "policies.json"
        dest: "/etc/chromium/policies/managed/"
      tags: policies

    - name: "copy: script to stop game cheating"
      copy:
        src: g
        dest: /usr/local/sbin/g
        mode: 0755
        owner: root
        group: root

    - name: "copy: cron job to stop game cheating"
      copy:
        content: |
          * * * * *            root  /usr/local/sbin/g
          30 20 * * *          root  /usr/local/sbin/g
          * 21-23,0-12 * * 1-5 root  /usr/local/sbin/g
          * 21-23,0-7 * * 6-7  root  /usr/local/sbin/g
          * 0-7 * * *          root  rm -f /tmp/allowed
        dest: /etc/cron.d/g
        mode: 0644
        owner: root
        group: root
      tags: policies

    # exception after the Chromium blacklist and whitelist.
    - name: "set full hostname with domain name"
      lineinfile:
        line: "{{ item }}"
        dest: /etc/hosts
        state: present
      with_items:
        - "127.0.0.1	scratch.mit.edu"
        - "127.0.0.1	tvthek.orf.at"

    - name: "file: gmpc config dir"
      file:
        dest: "{{ userhome }}/.config"
        state: directory
        owner: "{{ user }}"
        group: "{{ user }}"
        mode: 0700
    - name: "file: gmpc config dir"
      file:
        dest: "{{ userhome }}/.config/gmpc"
        state: directory
        owner: "{{ user }}"
        group: "{{ user }}"
        mode: 0700
    - name: "copy: mpd profile"
      copy:
        owner: "{{ user }}"
        group: "{{ user }}"
        mode: 0600
        content: |
          # Do not edit this file!!!

          [Default]
          hostname="10.4.0.1"
          name="blinky"
          password="musicnonstop"
          portnumber="19907"
          useauth="1"
          music directory=""
          db update time="1640616541"
          []
        dest: "{{ userhome }}/.config/gmpc/profiles.cfg"

    - name: "Trickster Cards app"
      template:
        src: app.desktop.j2
        dest: /usr/share/applications/trickster-cards.desktop
        mode: 0644
        owner: root
        group: root
      vars:
        - name: Trickster Cards
        - url: https://www.trickstercards.com/

    - name: "Roland 50 Studio app"
      template:
        src: app.desktop.j2
        dest: /usr/share/applications/roland-50-studio.desktop
        mode: 0644
        owner: root
        group: root
      vars:
        - name: Roland 50 Studio
        - url: https://roland50.studio

    - name: "file: create dir to manually install GNOME desktop files"
      file:
        path: "{{ userhome }}/.local/share/applications"
        state: directory
        mode: 0700
        owner: "{{ user }}"
        group: "{{ user }}"

    # requires ansible 2.9
    - name: "flatpak_remote: add flathub"
      flatpak_remote:
        name: flathub
        flatpakrepo_url: https://flathub.org/repo/flathub.flatpakrepo

    - name: "flatpak: install 0 A.D."
      flatpak:
        name: com.play0ad.zeroad

    - name: "symlink 0 A.D. .desktop file"
      file:
        src: /var/lib/flatpak/exports/share/applications/com.play0ad.zeroad.desktop
        dest: "{{ userhome }}/.local/share/applications/com.play0ad.zeroad.desktop"
        owner: "{{ user }}"
        group: "{{ user }}"
        state: link

    - name: "flatpak: install LeoCAD"
      flatpak:
        name: org.leocad.LeoCAD

    - name: "symlink LeoCAD .desktop file"
      file:
        src: /var/lib/flatpak/exports/share/applications/org.leocad.LeoCAD.desktop
        dest: "{{ userhome }}/.local/share/applications/org.leocad.LeoCAD.desktop"
        owner: "{{ user }}"
        group: "{{ user }}"
        state: link

    - name: "flatpak: install DeltaChat"
      flatpak:
        name: chat.delta.desktop

    - name: "symlink DeltaChat .desktop file"
      file:
        src: /var/lib/flatpak/exports/share/applications/chat.delta.desktop.desktop
        dest: "{{ userhome }}/.local/share/applications/chat.delta.desktop.desktop"
        owner: "{{ user }}"
        group: "{{ user }}"
        state: link

    - name: "flatpak: install Pd-extended"
      flatpak:
        name: info.puredata.Pd-extended

    - name: "symlink Pd-extended .desktop file"
      file:
        src: /var/lib/flatpak/exports/share/applications/info.puredata.Pd-extended.desktop
        dest: "{{ userhome }}/.local/share/applications/info.puredata.Pd-extended.desktop"
        owner: "{{ user }}"
        group: "{{ user }}"
        state: link
